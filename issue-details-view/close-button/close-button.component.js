(function (angular) {

    return angular
        .module('JSIssueTracker')
        .component('closeButton', {
            templateUrl: 'issue-details-view/close-button/close-button.template.html',
        });
})(angular);