(function (angular) {

    class controller {
        constructor(handleDataService) {
            this.handleDataService = handleDataService;
        }
    }

    return angular
        .module('JSIssueTracker')
        .component('issueDetailsView', {
            templateUrl: 'issue-details-view/issue-details-view.template.html',
            controller
        });
})(angular);