(function (angular) {

    return angular
        .module('JSIssueTracker')
        .component('deleteButton', {
            templateUrl: 'issue-details-view/delete-button/delete-button.template.html',
        });
})(angular);