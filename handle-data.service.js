(function (angular, chance) {

    class HandleDataService {
        constructor() {
            this.issue = {};
            this.issues = [];
        }

        saveIssue() {
            const issue = Object.assign({}, this.issue, {
                id: chance.guid(),
                status: 'open'
            });

            this.issues
                .push(issue);
        }

        closeIssue(id) {
            this.issues.forEach(issue => {
                if (issue.id === id) {
                    issue.status = 'closed';
                }
            });
        }

        deleteIssue(id) {
            this.issues = this.issues.filter(issue => issue.id !== id);
        }
    }

    return angular
        .module('JSIssueTracker')
        .service('handleDataService', HandleDataService);

})(angular, chance);