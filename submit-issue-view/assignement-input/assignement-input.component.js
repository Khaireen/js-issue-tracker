(function (angular) {

    class controller {
        constructor(handleDataService) {
            this.handleDataService = handleDataService;
        }
    }

    return angular
        .module('JSIssueTracker')
        .component('assignementInput', {
            templateUrl: 'submit-issue-view/assignement-input/assignement-input.template.html',
            controller
        });

})(angular);