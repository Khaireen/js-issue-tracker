(function (angular) {
    
    class controller {
        constructor(handleDataService) {
            this.handleDataService = handleDataService;
        }
    }

    return angular
        .module('JSIssueTracker')
        .component('descriptionInput', {
            templateUrl: 'submit-issue-view/description-input/description-input.template.html',
            controller
        });

})(angular);