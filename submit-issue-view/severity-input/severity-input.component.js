(function (angular) {
    
    class controller {
        constructor(handleDataService) {
            this.handleDataService = handleDataService;
        }
    }

    return angular
        .module('JSIssueTracker')
        .component('severityInput', {
            templateUrl: 'submit-issue-view/severity-input/severity-input.template.html',
            controller
        });

})(angular);