(function (angular, chance) {

    class controller {
        constructor(handleDataService) {
            this.handleDataService = handleDataService;
        }
    }

    return angular
        .module('JSIssueTracker')
        .component('submitIssueView', {
            templateUrl: 'submit-issue-view/submit-issue-view.template.html',
            controller
        });

})(angular, chance);