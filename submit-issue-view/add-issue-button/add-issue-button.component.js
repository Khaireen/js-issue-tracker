(function (angular) {

    class controller {
        constructor(handleDataService) {
            this.handleDataService = handleDataService;
        }
    }

    return angular
        .module('JSIssueTracker')
        .component('addIssue', {
            templateUrl: 'submit-issue-view/add-issue-button/add-issue-button.template.html',
            controller
        });

})(angular);